angular.module('peanuthub-custom-keyboard.components', []);
angular.module('peanuthub-custom-keyboard.services', []);
angular.module('peanuthub-custom-keyboard', [
    'peanuthub-custom-keyboard.templates',
    'peanuthub-custom-keyboard.services',
    'peanuthub-custom-keyboard.components'
]);
