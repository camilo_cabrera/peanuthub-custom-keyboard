angular.module('peanuthub-custom-keyboard.services')
    .provider('$peanuthubCustomKeyboard', function() {
        var $this = this;
        var _customKeyboards = {};

        this.addCustomKeyboard = function(keyboardName, configuration) {
            _customKeyboards[keyboardName] = configuration;
        };

        this.$get = function($q, $log) {
            var self = {};

            self.getKeyboards = function() {
                if (!_customKeyboards) {
                    throw Error("KEYBOARDS_NOT_INITIALIZED");
                }
                return _customKeyboards;
            };

            return self;
        };

    });
